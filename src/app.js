require('dotenv').config();

const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const faker = require('faker');
const route = require('./route');

const HOST = process.env.HOST;
const PORT = process.env.PORT;

const app = express();

app.use(cors());
app.use(morgan('dev'));

app.get('/api/index', async (req, res, next) => {
  if((Math.ceil(Math.random() * 1000) === 1)) {
    await route.setNextRoute('');
    return res.status(200).json({
      content: faker.lorem.sentence(20)
    });
  }
  const nextRoute = faker.random.uuid();
  await route.setNextRoute(nextRoute);
  return res.status(200).json({url:`http://${HOST}:${PORT}/api/${nextRoute}`});
});

app.get('/api/:path', async (req, res, next) => {
  const nextRoute = await route.getNextRoute();
  if(req.params.path === nextRoute) {
    if((Math.ceil(Math.random() * 1000) === 1)) {
      await route.setNextRoute('');
      return res.status(200).json({
        content: faker.lorem.paragraphs(3),
      });
    }
    const nextRoute = faker.random.uuid();
    await route.setNextRoute(nextRoute);
    return res.status(200).json({url:`http://${HOST}:${PORT}/api/${nextRoute}`});
  }
  return next();
});

app.use((req, res) => {
  return res.status(404).send('Not found');
});

app.listen(PORT, () => {
  console.log(`Running on ${HOST}:${PORT}`);
});
