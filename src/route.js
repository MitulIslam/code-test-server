const redisClient = require('./redisClient');

async function setNextRoute(route) {
  await redisClient.setAsync(`next/route`, route);
}

async function getNextRoute() {
  const nextRoute = await redisClient.getAsync(`next/route`);
  return nextRoute;
}

module.exports = {
  setNextRoute,
  getNextRoute
}